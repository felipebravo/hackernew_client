import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  configUrl = 'http://localhost:3003';

  auth() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': '*/*',
      })
    };
    let path = '/api/auth'
    return this.http.post(this.configUrl + path, httpOptions);
  }

  gethits(tk) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    const form = {
      token: tk
    }

    let path = '/api/getnews'
    return this.http.post(this.configUrl + path, JSON.stringify(form), httpOptions);
  }

  deleterow(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    const form = {
      token: localStorage.getItem('token'),
      id: id
    }

    let path = '/api/updatenew';
    return this.http.post(this.configUrl + path, JSON.stringify(form), httpOptions);
  }
}
