import {Pipe, PipeTransform} from '@angular/core';
import { DatePipe } from '@angular/common';


@Pipe({
    name: 'niceDateFormatPipe',
})
export class niceDateFormatPipe implements PipeTransform {
    transform(value) {
       
       var _value = Date.parse(value);
       //console.log(_value);
       
       var dif = Math.floor( ( (Date.now() - _value) / 1000 ) / 86400 );
       
       if ( dif < 30 ){
            return convertToNiceDate(value);
       }else{
           var datePipe = new DatePipe("en-US");
           value = datePipe.transform(value, 'MMM-dd-yyyy H:mm');
           return value;
       }
    }
}

function convertToNiceDate(time: string) {
    var date = new Date(time),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        daydiff = Math.floor(diff / 86400);

    if (isNaN(daydiff) || daydiff < 0 || daydiff >= 31)
        return '';

    return daydiff == 0 && (
        diff < 60 && "Justo Ahora" ||
        diff < 120 && "1 minuto átras" ||
        diff < 3600 && Math.floor(diff / 60) + " minutos átras" ||
        diff < 7200 && "1 Hora átras" ||
        diff < 86400 && Math.floor(diff / 3600) + " horas átras") ||
        daydiff == 1 && "Ayer" ||
        daydiff < 7 && daydiff + " Días átras" ||
        daydiff < 31 && Math.ceil(daydiff / 7) + " Semanas átras";
}
