import { Component, OnInit } from '@angular/core';

import { ApiService } from '../service/api.service';



@Component({
  selector: 'app-hits-list',
  templateUrl: './hits-list.component.html',
  styleUrls: ['./hits-list.component.css']
})
export class HitsListComponent implements OnInit {
  hits: any;
  view: any;
  constructor(public api: ApiService) {
    this.hits = [];
  }

  ngOnInit(): void {
    //call auth get token
    this.api.auth().subscribe((response) => {

      let token = response['token'];
      localStorage.setItem('token', response['token']);

      //call Api
      this.api.gethits(token).subscribe((response) => {
        this.hits = response;
        //sort by date created_at
        this.hits.sort((b, a) => a.created_at.localeCompare(b.created_at));

      })
    })
  }



  deleterow(id) {
    this.api.deleterow(id).subscribe((response) => {
      console.log(response);
    })
  }

  //open new tab
  goToLink(url: string) {
    console.log(url);
    if (url) {
      window.open(url, "_blank");
    } else {
      alert("Sin URL");
    }
  }
}
